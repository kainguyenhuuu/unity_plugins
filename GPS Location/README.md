GPS Position

1. GPS Require
-Mô tả:
Kiểm tra chế độ bật GPS của thiết bị và yêu cầu bật nếu cần.

-Cài đặt: 
https://docs.google.com/spreadsheets/d/1CJjdw7yqbeB83X6wdAQJzl3byAc03LfZGRgx4gFurK4/edit#gid=0


2. GPS Location
-Mô tả:
Cung cấp vị trí GPS hiện tại của thiết bị.

-Cài đặt: 
https://docs.google.com/spreadsheets/d/1jue37cV9PTLDC1f_sPls7yJ9JErozFLcjt8vb3YfGXc/edit#gid=0