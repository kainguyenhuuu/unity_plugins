﻿using UnityEngine;
using UnityEngine.UI;

public class Test : MonoBehaviour
{
    public Text Text;
    public GPSLocation GPSLocation;

    // Update is called once per frame
    void Update()
    {
        Text.text = GPSLocation.Location[0]+" "+GPSLocation.Location[1];
    }
}
