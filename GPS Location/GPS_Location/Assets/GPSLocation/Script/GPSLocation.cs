﻿using UnityEngine;

#if UNITY_ANDROID
using DeadMosquito.AndroidGoodies;
#endif

public class GPSLocation : MonoBehaviour
{
    public double[] Location;

    void Awake()
    {
        Location = new double[2];
        Location[0] = 0;
        Location[1] = 0;
    }

    #if UNITY_ANDROID
    void Start()
    {
        OnStartTrackingLocation();
    }

    public void OnStartTrackingLocation()
    {
        AGPermissions.ExecuteIfHasPermission(AGPermissions.ACCESS_FINE_LOCATION, StartLocationTacking, 
            () => {});
    }

    void StartLocationTacking()
    {
        // Minimum time interval between location updates, in milliseconds.
        const long minTimeInMillis = 0;
        // Minimum distance between location updates, in meters.
        const float minDistanceInMetres = 0;
        AGGPS.RequestLocationUpdates(minTimeInMillis, minDistanceInMetres, OnLocationChanged);
    }

    void OnLocationChanged(AGGPS.Location location)
    {
        Location[0] = location.Latitude;
        Location[1] = location.Longitude;
    }
    #endif
}
