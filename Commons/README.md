Commons Functions

1. HTTPRequest: Các hàm hỗ trợ Request HTTP.
2. PrefabFunctions: Các hàm hỗ trợ xử lý Prefab.
3. SimpleJSON: Thư viện decode string thành JSON và encode JSON thành string.
4. XXTEA: Phương thức bảo mật XXTEA.

