﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public static class PrefabFunctions
{
    /* -------------------------- */
	/* Instantiate Prefab */
	/* -------------------------- */
    public static GameObject InstantiatePrefab(GameObject prefab, string name, Transform parent, MonoBehaviour MonoBehaviour)
    {
        GameObject p_prefab_item = new GameObject();
		
        p_prefab_item = MonoBehaviour.Instantiate(prefab, parent, false);
		p_prefab_item.name = name;

        foreach(GameObject ob in SceneManager.GetActiveScene().GetRootGameObjects())
        {
            if(ob.name == "New Game Object")
            {
                MonoBehaviour.DestroyImmediate(ob);
            }
        }
        return p_prefab_item;
    }

}
