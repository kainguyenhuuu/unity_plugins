﻿using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.Networking;

public static class HTTPRequest
{
    /* -------------------------- */
    /* Request Get Protocol */
    /* -------------------------- */
    public static void Get(string url, Dictionary<string, string> header, MonoBehaviour MonoBehaviour, Action<SimpleJSON.JSONNode> Callback)
    {
        UnityWebRequest request = UnityWebRequest.Get(url);
        MonoBehaviour.StartCoroutine(Request(url, request, header, Callback));
    }

    /* -------------------------- */
    /* Request Post Protocol */
    /* -------------------------- */
    public static void Post(string url, Dictionary<string, string> header, SimpleJSON.JSONNode data, MonoBehaviour MonoBehaviour, Action<SimpleJSON.JSONNode> Callback)
    {
        WWWForm form = new WWWForm();
        form.AddField("data", data.ToString());
        UnityWebRequest request = UnityWebRequest.Post(url, form);

        MonoBehaviour.StartCoroutine(Request(url, request, header, Callback));
    }

    /* -------------------------- */
    /* Request */
    /* -------------------------- */
    public static IEnumerator Request(string url, UnityWebRequest request, Dictionary<string, string> header, Action<SimpleJSON.JSONNode> Callback)
    {
        if(header != null)
        {
            foreach(string key in header.Keys)
            {
                request.SetRequestHeader (key, header[key]);
            }
        }

        using (request)
        {
            yield return request.SendWebRequest();
            if (request.isNetworkError)
            {
                Debug.Log(url+" "+request.error);
            }
            else if (request.isHttpError)
            {
                Debug.Log(url+" "+request.error);
            }
            else
            {
                // Debug.Log(url+": "+request.downloadHandler.text);
                Callback(SimpleJSON.JSON.Parse(request.downloadHandler.text));
            }
        }
    }

    /* -------------------------- */
    /* Request Get Sprite */
    /* -------------------------- */
    public static IEnumerator GetSprite(string link, Vector2 pivot, MonoBehaviour MonoBehaviour, Action<Texture2D> Callback)
    {
        using (UnityWebRequest request = UnityWebRequestTexture.GetTexture(link))
        {
            yield return request.SendWebRequest();

            if (request.isNetworkError || request.isHttpError)
            {
                Debug.Log(link+": "+request.error);
                if(request.error == "Received no data in response")
                {
                    MonoBehaviour.StartCoroutine(GetSprite(link, pivot, MonoBehaviour, Callback));
                }
            }
            else
            {
                Callback(DownloadHandlerTexture.GetContent(request));
            }
        }
    }

}
