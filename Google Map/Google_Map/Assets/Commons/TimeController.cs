﻿using System;
using System.Collections;
using UnityEngine;

public class TimeController
{
    public static IEnumerator Wait(float time, Action Callback)
    {
        yield return new WaitForSeconds(time);
        Callback();
    }

    public static IEnumerator WaitLoop(float time, int from, int to, int step, Action<int> Inside, Action Outside)
    {
        for(int i=from; i<to; i += step)
        {
            Inside(i);
            yield return new WaitForSeconds(time);
        }
        Outside();
    }
}
