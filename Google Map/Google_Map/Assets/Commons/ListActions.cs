﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ListActions
{
    public static List<int> ListInt_Not(List<int> big, List<int> small)
    {
        List<int> all = new List<int>(big);
        foreach(int ele in small)
        {
            if(big.Contains(ele))
                all.Remove(ele);
        }
        return all;
    }

    public static List<string> ListString_Not(List<string> big, List<string> small)
    {
        List<string> all = new List<string>(big);
        foreach(string ele in small)
        {
            if(big.Contains(ele))
                all.Remove(ele);
        }
        return all;
    }
}
