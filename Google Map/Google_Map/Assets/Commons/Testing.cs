﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Testing : MonoBehaviour
{
    public bool Test;
    void Start()
    {
        if(!Test)
            Destroy(this.gameObject);

    }

    void  Update() 
    {
        if(Show_FPS)
            ShowFPS();
        if(Show_Map_Count)
            ShowMapsCount();
        if(Show_Map_Count)
            ShowCamPosition();
        
    }

    public bool Show_FPS;
    public Text FPS_Dipslay;
    void ShowFPS()
    {
		  FPS_Dipslay.text = string.Format("{0:0.0} ms ({1:0.} fps)", Time.deltaTime * 1000.0f, 1.0f / Time.deltaTime);
    }

    public bool Show_Map_Count;
    public Text Map_Count;
    public Transform Map_Root;
    void ShowMapsCount()
    {
		  Map_Count.text = Map_Root.childCount.ToString();
    }

    public bool Show_Cam_Pos;
    public Text Cam_Pos;
    public Transform cam;
    void ShowCamPosition()
    {
		  Cam_Pos.text = cam.localPosition.ToString();
    }
}
