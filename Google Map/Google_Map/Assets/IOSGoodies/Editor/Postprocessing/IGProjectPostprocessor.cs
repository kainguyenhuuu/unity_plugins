﻿using System;
using System.IO;
using DeadMosquito.IosGoodies.Internal;
using UnityEngine;

namespace DeadMosquito.IosGoodies.Editor
{
	using UnityEditor;
	using UnityEditor.Callbacks;
	using UnityEditor.iOS.Xcode;

	public static class IGProjectPostprocessor
	{
		public const string CameraUsageDescription = "NSCameraUsageDescription";
		public const string PhotoLibraryUsageDescription = "NSPhotoLibraryUsageDescription";
		public const string PhotoLibraryAddUsageDescription = "NSPhotoLibraryAddUsageDescription";
		public const string FaceIdUsageDescription = "NSFaceIDUsageDescription";
		public const string CalendarsUsageDescription = "NSCalendarsUsageDescription";
		public const string RemindersUsageDescriptionKey = "NSRemindersUsageDescription";

		[PostProcessBuild(2000)]
		public static void OnPostProcessBuild(BuildTarget target, string path)
		{
			if (target != BuildTarget.iOS)
			{
				return;
			}

			try
			{
				var plistInfoFile = new PlistDocument();

				var infoPlistPath = Path.Combine(path, "Info.plist");
				plistInfoFile.ReadFromString(File.ReadAllText(infoPlistPath));

				AddFeatureUsageDescriptions(plistInfoFile);

				File.WriteAllText(infoPlistPath, plistInfoFile.WriteToString());
			}
			catch (Exception e)
			{
				Debug.LogException(e);
			}
		}

		static void AddFeatureUsageDescriptions(PlistDocument plist)
		{
			SetIfRequired(plist, IosGoodiesSettings.IsImagePickerEnabled, CameraUsageDescription, IosGoodiesSettings.PhotoPickerUsageDescription);
			SetIfRequired(plist, IosGoodiesSettings.IsImagePickerEnabled, PhotoLibraryUsageDescription, IosGoodiesSettings.ImagePickerUsageDescription);
			SetIfRequired(plist, IosGoodiesSettings.IsImagePickerEnabled, PhotoLibraryAddUsageDescription, IosGoodiesSettings.AddPhotoToGalleryUsageDescription);
			
			SetIfRequired(plist, IosGoodiesSettings.IsBiometricAuthEnabled, FaceIdUsageDescription, IosGoodiesSettings.BiometricsUsageDescription);
			
			SetIfRequired(plist, IosGoodiesSettings.IsCalenderAndRemindersEnabled, CalendarsUsageDescription, IosGoodiesSettings.CalendarUsageDescription);
			SetIfRequired(plist, IosGoodiesSettings.IsCalenderAndRemindersEnabled, RemindersUsageDescriptionKey, IosGoodiesSettings.RemindersUsageDescription);
		}

		static void SetIfRequired(PlistDocument plist, bool isEnabled, string description, string usageDescription)
		{
			if (!isEnabled)
			{
				return;
			}

			if (!plist.root.values.ContainsKey(description))
			{
				plist.root.AsDict().SetString(description, usageDescription);
			}
		}
	}
}