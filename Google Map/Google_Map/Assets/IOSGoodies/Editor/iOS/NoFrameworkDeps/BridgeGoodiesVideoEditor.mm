#pragma clang diagnostic push
#pragma ide diagnostic ignored "OCUnusedGlobalDeclarationInspection"


#import "GoodiesUtils.h"

extern "C" {
void _editVideoAtPath(const char *path, bool animated) {
    UIVideoEditorController* videoEditor = [[UIVideoEditorController alloc] init];

    NSString *videoPath = [GoodiesUtils createNSStringFrom:path];

    if ([UIVideoEditorController canEditVideoAtPath:videoPath]) {
        videoEditor.videoPath = videoPath;
        videoEditor.videoMaximumDuration = 10.0;

        [UnityGetGLViewController() presentViewController:videoEditor animated:animated completion:nil];
    } else {
        NSLog(@"can't edit video at %@", videoPath);
    }
}

bool _canEditVideoAtPath(const char *path) {
    NSString *videoPath = [GoodiesUtils createNSStringFrom:path];
    return [UIVideoEditorController canEditVideoAtPath:videoPath];
}
}

#pragma clang diagnostic pop
