﻿using System;
using DeadMosquito.IosGoodies.Internal;

#if UNITY_IOS
namespace DeadMosquito.IosGoodies
{
	using System.Runtime.InteropServices;
	using JetBrains.Annotations;

	/// <summary>
	/// Allows to opens system video editor
	/// </summary>
	[PublicAPI]
	public static class IGVideoEditor
	{
		/// <summary>
		/// 
		/// </summary>
		/// <param name="videoPath">Path to the video to check whether it can be edited</param>
		/// <returns><c>true</c> if video at provided path can be edited, false otherwise ; otherwise, <c>false</c></returns>
		public static bool CanEditVideoAtPath([NotNull] string videoPath)
		{
			if (IGUtils.IsIosCheck())
			{
				return false;
			}
			
			return _canEditVideoAtPath(videoPath);
		}

		public static void EditVideo([NotNull] string videoPath, int videoMaximumDuration = 600)
		{
			if (videoPath == null)
			{
				throw new ArgumentNullException("videoPath");
			}

			if (IGUtils.IsIosCheck())
			{
				return;
			}

			_editVideoAtPath(videoPath);
		}

		[DllImport("__Internal")]
		static extern void _editVideoAtPath(string path);
		
		[DllImport("__Internal")]
		static extern bool _canEditVideoAtPath(string path);
	}
}
#endif