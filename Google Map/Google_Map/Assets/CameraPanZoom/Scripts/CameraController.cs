﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public Camera camera;
    public float scrollwheel_speed = 3f;
    public float two_touch_speed = 0.01f;

    Vector3 TouchStart;
    void Update()
    {
        Pan(); 
        TwoTouch();
        Zoom(Input.GetAxis("Mouse ScrollWheel")*scrollwheel_speed);
    }


    /* -------------------------- */
    /* Pan Camera */
    /* -------------------------- */
    void Pan()
    {
        if(Input.touchCount < 2)
        {
            if(Input.GetMouseButtonDown(0))
            {
                TouchStart = camera.ScreenToWorldPoint(Input.mousePosition);
            }

            if(Input.GetMouseButton(0))
            {
                Vector3 direction = TouchStart - camera.ScreenToWorldPoint(Input.mousePosition);
                if(Mathf.Abs(direction.x) < camera.orthographicSize/3 && Mathf.Abs(direction.y) < camera.orthographicSize/3)
                {
                    camera.transform.Translate(direction);
                    TouchStart = camera.ScreenToWorldPoint(Input.mousePosition);
                }
            }
        }
    }


    /* -------------------------- */
    /* Zoom Camera */
    /* -------------------------- */
    public float zoomOutMin = 1;
    public float zoumOutMax = 20;
    void TwoTouch()
    {
        if(Input.touchCount == 2)
        {
            Touch touchZero = Input.GetTouch(0);
            Touch touchOne = Input.GetTouch(1);

            Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
            Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;

            float prevMagnitude = (touchZeroPrevPos - touchOnePrevPos).magnitude;
            float currentMagnitude = (touchZero.position - touchOne.position).magnitude;

            float difference = currentMagnitude - prevMagnitude;

            Zoom(difference*two_touch_speed);
        }
    }

    void Zoom(float value)
    {
        // camera.orthographicSize = Mathf.Clamp(camera.orthographicSize - value, zoomOutMin, zoumOutMax);
    }

}