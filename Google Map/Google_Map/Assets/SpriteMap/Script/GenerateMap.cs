﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class GenerateMap : MonoBehaviour
{
    public int block_size;
    public double scale, top_bottom, left_right;
    List<int> corner_x, corner_y;
    public GameObject prefab_maps;
    public Dictionary<string, GameObject> current;
    List<string> new_list;
    public Transform root_maps;
    

    void Awake()
    {
        current  = new Dictionary<string, GameObject>();
        corner_x = new List<int>();
        corner_y = new List<int>();
        new_list = new List<string>();

        //Map 5x5
        for(int x=-block_size; x<=block_size; x++)
        {
            for(int y=-block_size; y<=block_size; y++)
            {
                corner_x.Add(x);
                corner_y.Add(y);
            }
        }
    }
    /*--------------------------------*/
    /*--- Check Request Done ---*/
    /*--------------------------------*/
    public bool Check()
    {
        if(root_maps.childCount <= 0)
            return false;
        else
            return true;
    }

    /*---------------------------------*/
    /*--- Get Map Sprites from HTTP ---*/
    /*---------------------------------*/
    public void RequestHTTP(MapData map_data)
    {
        new_list.Clear();
        // Generate Map Block each 0.05f seconds
        // StartCoroutine(TimeController.WaitLoop(0.05f, 0, corner_x.Count, 1, (i)=>{
        for(int i=0; i<corner_x.Count; i++)
        {
            int index = i;
            string url, name;
            var x = map_data.Pos_X+(corner_x[i]*map_data.Range);
            var y = map_data.Pos_Y+(corner_y[i]*map_data.Range);
            name = x.ToString("N6")+","+y.ToString("N6");
            new_list.Add(name);
            
            if(!current.ContainsKey(name))
            {
                if(map_data.Google)
                    url = map_data.URL+(float)y+","+(float)x;
                else
                    url = "";

                // Create Map Block Data
                MapBlock map_block = new MapBlock();
                map_block.Scale     = scale;
                map_block.Top_Bottom= top_bottom;
                map_block.Left_Right= left_right;
                map_block.Range     = map_data.Range;
                map_block.Pos_X     = x-map_data.Center_X;
                map_block.Pos_Y     = y-map_data.Center_Y;
                map_block.Z_Index   = ((y-map_data.Center_Z)/map_data.Range)*0.01f;

                // Request map Sprite from Server
                GameObject map = PrefabFunctions.InstantiatePrefab(prefab_maps, name, root_maps, this);
                current[name] = map;
                StartCoroutine(HTTPRequest.GetSprite(url, new Vector2(0.5f, 0.5f), this, (texture)=>{
                    if(current.ContainsKey(name))
                    {
                        // Add Sprite to Map Block Data
                        map_block.Texture = texture;

                        // Generate Prefab from Block Data
                        map.GetComponent<Maps>().Setup(map_block);
                    }
                }));
            }
        // },()=>{
            // Remove old Map Blocks;
            // RemoveMap(map_data);
        // }));
        }
        // Remove old Map Blocks;
        RemoveMap(map_data);
        
    }

    /*-------------------------*/
    /*--- Remove Map Blocks ---*/
    /*-------------------------*/
    public void RemoveMap(MapData map_data)
    {
        foreach (string remove in ListActions.ListString_Not(new List<string>(current.Keys), new_list))
        {
            if(current.ContainsKey(remove))
            {
                DestroyImmediate(current[remove]);
                current.Remove(remove); 
            }
        }
    }

    /*----------------------------------*/
    /*--- Position of Camera in Maps ---*/
    /*----------------------------------*/
    public double CameraPositionConverter(double center, double cam_pos, double range)
    {
        return center+((cam_pos*range)/scale);
    }

    /*-------------------------------------*/
    /*--- Position of Maps in Cordinate ---*/
    /*-------------------------------------*/
    double MapsPositionConverter(double pos, double range, double margin, double size)
    {
        // Debug.Log(pos+" "+(pos-((Mathf.Round(pos_x/range)*margin)*(range/size))));
        // return pos-((Mathf.Round(pos_x/range)*margin)*(range/size));
        return 0;
    }
}

public sealed class MapBlock
{
    Texture2D texture;
    double range;
    double scale = 1.0f;
    double pos_x, pos_y;
    double z_index;
    double left_right, top_bottom;

    public Texture2D Texture
    {
        set { texture = value; }
        get { return texture; }
    }
    public double Range
    {
        set { range = value; }
        get { return range; }
    }

    public double Scale
    {
        set { scale = value; }
        get { return scale; }
    }

    public double Pos_X
    {
        set { pos_x = value; }
        get { return pos_x; }
    }

    public double Pos_Y
    {
        set { pos_y = value; }
        get { return pos_y; }
    }

    public double Z_Index
    {
        set { z_index = value; }
        get { return z_index; }
    }

    public double Left_Right
    {
        set { left_right = value;}
        get { return left_right; }
    }

    public double Top_Bottom
    {
        set { top_bottom = value; }
        get { return top_bottom; }
    }

    public Vector3 ScaleCoordinate()
    {
        return new Vector3((float)scale, (float)scale, 1.0f);
    }

    public Vector3 PositionCoordinate()
    {
        double x = Math.Round(pos_x/range)*(scale-left_right);
        double y = Math.Round(pos_y/range)*(scale-top_bottom);
        return new Vector3((float)x, (float)y, (float)z_index);
    }
}