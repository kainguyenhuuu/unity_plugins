﻿using UnityEngine;
using UnityEngine.UI;

public class Maps : MonoBehaviour
{
    public Transform trans;
    public Renderer mat;
    public void Setup(MapBlock map_block)
    {
        mat.material.mainTexture = map_block.Texture;
		trans.localScale = map_block.ScaleCoordinate();
		trans.localPosition = map_block.PositionCoordinate();
    }
}