﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapManager : MonoBehaviour
{
    public float ortho_size;
    public int zoom;
    public CameraController CameraController;

    public bool google_map;
    public GenerateMap GenerateMap;    
    
    MapData map_data;
    void Awake()
    {
        map_data = new MapData();
    }

    /*-----------------------------------*/
    /*--- Generate map data from HTTP ---*/
    /*-----------------------------------*/
    public void GenerateFromHTTP(string url, double pos_x, double pos_y, double range)
    {
        Reset();

        map_data.Range  = range;
        map_data.URL    = url;

        map_data.Center_X   = pos_x;
        map_data.Center_Y   = pos_y;
        map_data.Center_Z   = pos_y;

        map_data.Pos_X  = pos_x;
        map_data.Pos_Y  = pos_y;

        map_data.Google = google_map;
        GenerateMap.RequestHTTP(map_data);

        CameraController.camera.transform.position = new Vector3(0, 0, -5);
    }
    void Update()
    {
        if(GenerateMap.Check())
        {
            CameraMoving(CameraController.camera.transform.position.x, CameraController.camera.transform.position.y);
        }
    }

    /*---------------------------------------------*/
    /*--- Generate map data when Camera Moving ---*/
    /*---------------------------------------------*/
    public void CameraMoving(double cam_x, double cam_y)
    {
        if(GenerateMap.CameraPositionConverter(map_data.Center_X, cam_x, map_data.Range) <= map_data.Min_X || 
        GenerateMap.CameraPositionConverter(map_data.Center_X, cam_x, map_data.Range) >= map_data.Max_X ||
        GenerateMap.CameraPositionConverter(map_data.Center_Y, cam_y, map_data.Range) <= map_data.Min_Y ||
        GenerateMap.CameraPositionConverter(map_data.Center_Y, cam_y, map_data.Range) >= map_data.Max_Y)
        {
            if(GenerateMap.CameraPositionConverter(map_data.Center_X, cam_x, map_data.Range) <= map_data.Min_X)
            {
                map_data.Pos_X -= map_data.Range;
            }
            if(GenerateMap.CameraPositionConverter(map_data.Center_X, cam_x, map_data.Range) >= map_data.Max_X)
            {
                map_data.Pos_X += map_data.Range;
            }
            if(GenerateMap.CameraPositionConverter(map_data.Center_Y, cam_y, map_data.Range) <= map_data.Min_Y)
            {
                map_data.Pos_Y -= map_data.Range;
            }
            if(GenerateMap.CameraPositionConverter(map_data.Center_Y, cam_y, map_data.Range) >= map_data.Max_Y)
            {
                map_data.Pos_Y += map_data.Range;
            }
            GenerateMap.RequestHTTP(map_data);
        }
    }
    
    /*--------------------------*/
    /*--- Reset all map data ---*/
    /*--------------------------*/
    public void Reset()
    {
        foreach(Transform map in GenerateMap.root_maps)
        {
            Destroy(map.gameObject);
        }
        GenerateMap.current.Clear();
        CameraController.camera.orthographicSize = ortho_size;
    }
}


public sealed class MapData
{
    bool google;
    string url;
    double range;
    double center_x, center_y;
    double pos_x, pos_y;
    double min_x, max_x, min_y, max_y;
    double center_z;

    public string URL
    {
        set { url = value; }
        get { return url; }
    }
    public double Range
    {
        set { range = value; }
        get { return range; }
    }

    public double Center_X
    {
        set { center_x = value;}
        get { return center_x; }
    }

    public double Center_Y
    {
        set { center_y = value; }
        get { return center_y; }
    }

    public double Pos_X
    {
        set 
        { 
            pos_x = value; 
            min_x = pos_x-(range/2);
            max_x = pos_x+(range/2);
        }
        get { return pos_x; }
    }

    public double Pos_Y
    {
        set 
        { 
            pos_y = value; 
            min_y = pos_y-(range/2);
            max_y = pos_y+(range/2);
        }
        get { return pos_y; }
    }

    public bool Google
    {
        set { google = value; }
        get { return google; }
    }

    public double Min_X
    {
        get { return min_x; }
    }
    public double Max_X
    {
        get { return max_x; }
    }
    public double Min_Y
    {
        get { return min_y; }
    }
    public double Max_Y
    {
        get { return max_y; }
    }

    public double Center_Z
    {
        set { center_z = value; }
        get { return center_z; }
    }
}