﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoogleMapManager : MonoBehaviour
{
    public GPSLocation GPSLocation;
    public MapManager MapManager;

    GoogleAPIStaticMap GoogleMap;
    public bool marker;
    public float range;

    void Awake()
    {
        GoogleMap = new GoogleAPIStaticMap();
    }

    public void SetSize(int x, int y)
    {
        GoogleMap.SizeX = x;
        GoogleMap.SizeY = y;
    }

    public void SetZoom(int data)
    {
        GoogleMap.Zoom = data;
    }


    public void SetAPIKey(string data)
    {
        GoogleMap.APIKey = data;
    }

    public void SetPath(int thickness, string color, string line)
    {
        GoogleMap.PathThickness = thickness;
        GoogleMap.PathColor     = color;
        GoogleMap.Polyline      = line;
        GoogleMap.Marker        = marker;
    }

    public void Generate()
    {
        // GoogleMap.Latitude = GPSLocation.Latitude;
        // GoogleMap.Longitude = GPSLocation.Longitude;
        GoogleMap.Latitude = 10.78654321;
        GoogleMap.Longitude = 106.635;
        MapManager.GenerateFromHTTP(GoogleMap.ToString(), GoogleMap.Longitude, GoogleMap.Latitude,  range);
        // InvokeRepeating("UpdateCurrentMap", 0.5f, 10f);
    }

    void UpdateCurrentMap()
    {
        if(GoogleMap.Latitude != GPSLocation.Latitude && GoogleMap.Longitude != GPSLocation.Longitude)
        {
            GoogleMap.Latitude = GPSLocation.Latitude;
            GoogleMap.Longitude = GPSLocation.Longitude;
        }
    }

    void Update() 
    {
        // Debug.Log(MapManager.CameraController.camera.orthographicSize);
    }
}

public sealed class GoogleAPIStaticMap
{
    double latitude, longtitude;
    int zoom = 13;
    int size_x = 640, size_y = 640;
    int scale = 4;
    string map_type;
    string api_key = "";
    bool marker = false;
    string marker_settings = "";
    int path_thickness = 0;
    string path_color = "";
    string polyline = "";

    public double Latitude
    {
        set { latitude = value; }
        get { return latitude; }
    }
    public double Longitude
    {
        set { longtitude = value; }
        get { return longtitude; }
    }

    public int Zoom
    {
        set { zoom = value; }
        get { return zoom; }
    }

    public int SizeX
    {
        set { size_x = value; }
        get { return size_x; }
    }

    public int SizeY
    {
        set { size_y = value; }
        get { return size_y; }
    }

    public int Scale
    {
        set { scale = value;}
        get { return scale; }
    }

    public string MapType
    {
        set { map_type = value;}
        get { return map_type; }
    }

    public string APIKey
    {
        set { api_key = value;}
        get { return api_key; }
    }

    public bool Marker
    {
        set 
        { 
            marker = value;
            if(!marker)
                marker_settings = "";
            else
                marker_settings = "&style=feature:poi|element:labels|visibility:off";
        }
    }

    public int PathThickness
    {
        set { path_thickness = value;}
        get { return path_thickness; }
    }

    public string PathColor
    {
        set { path_color = value;}
        get { return path_color; }
    }

    public string Polyline
    {
        set { polyline = value;}
        get { return polyline; }
    }

    public override string ToString()
    {
        /*Validate*/
        if(api_key == "")
        {
            return "Please Assign Key";
        }

        string path = "";
        if(path_thickness != 0 && path_color != "" && polyline != "")
        {
            path = string.Format("&path=weight:{0}"+"%7Ccolor:{1}"+"%7Cenc:{2}",
                    path_thickness,
                    path_color,
                    polyline);
        }
        /*-------*/

        return string.Format("https://maps.googleapis.com/maps/api/staticmap?" +
                                "zoom={0}"+
                                "&size={1}"+
                                "&scale={2}"+
                                "&maptype={3}"+
                                "{4}"+
                                "&key={5}"+
                                "{6}"+
                                "&format=jpg"+
                                "&center=",
                    zoom,
					size_x+"x"+size_y,
					scale,
					map_type, 
                    path,
                    api_key,
                    marker_settings);
    }
}