﻿using UnityEngine;

#if UNITY_ANDROID
using DeadMosquito.AndroidGoodies;
#endif
public class GPSRequire : MonoBehaviour
{
    public GameObject EnableGPS;

    void Update()
    {
        if(AGGPS.IsGPSEnabled())
        {
            EnableGPS.SetActive(false);
        }
        else
        {
            EnableGPS.SetActive(true);
        }
    }

    public void OpenSettingGPS()
    {
        AGSettings.OpenSettingsScreen(AGSettings.ACTION_LOCATION_SOURCE_SETTINGS);
    }
}
